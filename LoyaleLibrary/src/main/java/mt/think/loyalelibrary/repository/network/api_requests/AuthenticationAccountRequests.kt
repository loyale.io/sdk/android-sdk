package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_body.ApiRequestRegisterPushToken
import mt.think.loyalelibrary.repository.models.api_response.ApiLoginResponseDataModel
import mt.think.loyalelibrary.repository.models.api_body.ApiChangePasswordDataClass
import mt.think.loyalelibrary.repository.models.api_body.ApiEmailDataClass
import mt.think.loyalelibrary.repository.models.api_body.ApiLoginRequestModel
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling

open class AuthenticationAccountRequests(
    private val baseUrl: String,
    private val applicationContext: Context
) {

    suspend fun login(
        schemeId: String,
        login: String,
        pass: String
    ): ResponseHolder<ApiLoginResponseDataModel?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .authUser(schemeId, ApiLoginRequestModel(login, pass))
        }
    }

    suspend fun registerPushToken(
        schemeId: String,
        token: String,
        userId: String
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .registerPushToken(
                    schemeId,
                    ApiRequestRegisterPushToken(userId, token)
                )
        }
    }

    suspend fun forgotPassword(
        schemeId: String,
        email: ApiEmailDataClass
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .forgotPassword(schemeId = schemeId, email = email)
        }
    }

    suspend fun changePassword(
        token: String,
        schemeId: String,
        passwordsData: ApiChangePasswordDataClass
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .changePassword(authHeader = BEARER + token, schemeId = schemeId, passwordsData = passwordsData)
        }
    }

}