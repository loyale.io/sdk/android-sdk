package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_body.ApiRequestMessage
import mt.think.loyalelibrary.repository.models.api_response.ApiSchemeResultView
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling

open class SchemeRequests(private val baseUrl: String, private val applicationContext: Context) {
    suspend fun getSchemeById(
        id: String
    ): ResponseHolder<ApiSchemeResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getSchemeById(id = id)
        }
    }
}