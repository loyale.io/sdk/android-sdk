package mt.think.loyalelibrary.repository.models.other

import mt.think.loyalelibrary.repository.models.api_response.ApiLevelResultView

data class ParentAdditionalFieldsView(
    val id: String?,
    val email: String?,
    val firstName: String?,
    val name: String?,
    val lastName: String,
    val dob: String?,
    val gender: Int,
    val areaCode: String?,
    val mobileNumber: String?,
    val addressLine1: String?,
    val addressLine2: String?,
    val town: String?,
    val state: String?,
    val postCode: String?,
    val country: String?,
    val marketingSub: Boolean,
    val barCode: String?,
    val updatedDate: String,
    val createdDate: String,
    val lastConnectedDate: String?,
    val lastTransactionDate: String?,
    val profileImageUrl: String?,
    val onHold: Boolean,
    val emailVerified: Boolean,
    val mobileNumberVerified: Boolean,
    val externalRefId: String?,
    val additionalCustomerFields: ArrayList<AdditionalCustomerFieldsResultView?>,
    val balance: PointBalanceSummaryResult
    )
