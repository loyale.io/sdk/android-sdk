package mt.think.loyalelibrary.repository.models.api_body

import mt.think.loyalelibrary.repository.models.api_response.ApiLevelResultView
import mt.think.loyalelibrary.repository.models.other.*

data class ApiCustomerInsertView(
    val firstName: String?,
    val lastName: String?,
    val dob: String?,
    val email: String?,
    val password: String?,
    val areaCode: String?,
    val mobileNumber: String?,
    val gender: Int, // enum 0-3
    val marketingSub: Boolean?,
    val profileImageUrl: String?,
    val addressLine1: String?,
    val addressLine2: String?,
    val town: String?,
    val state: String?,
    val postCode: String?,
    val country: String?,
    val referralCode: String?,
    val signUpPlatform: String?,
    val signUpPlatform2: SignUpPlatformView,
    val skipMailgunValidation: Boolean,
    val skipMobileUniqueValidation: Boolean,
    val externalRefId: String?,
    val parent: ParentAdditionalFieldsView,
    val customerDevices: CustomerDevicesInsertView
)
