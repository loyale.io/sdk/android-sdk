package mt.think.loyalelibrary.repository.models.api_response

import mt.think.loyalelibrary.repository.models.other.AdditionalCustomerFieldsResultView

data class ApiCustomerWithAdditionalFieldsResultView(
    val customer: ApiCustomerResultView,
    val additionalFields: ArrayList<AdditionalCustomerFieldsResultView?>,
    val updatedDate: String,
    val createdDate: String
)