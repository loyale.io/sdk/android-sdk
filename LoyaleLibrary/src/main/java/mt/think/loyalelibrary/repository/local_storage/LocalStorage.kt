package mt.think.loyalelibrary.repository.local_storage

import android.app.Activity
import android.content.Context
import com.google.gson.Gson
import mt.think.loyalelibrary.repository.models.other.UserIdAuthTokenLocalStorageDataModel

const val SHARED_PREF = "sharedPreferenses"
const val KEY_AUTH_TOKEN_USER_ID = "keyAuthToken"
const val KEY_USER_LOGIN = "keyUserLoginPass"
const val KEY_NOTIFICATION_TOKEN = "keyNotificationToken"


open class LocalStorage(val activity: Activity) {

    /**
     * saving auth token
     */
    fun saveUserIdAndAuthToken(userData: UserIdAuthTokenLocalStorageDataModel) {
        val sharedPref = activity.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString(KEY_AUTH_TOKEN_USER_ID, Gson().toJson(userData))
            apply()
        }
    }

    /**
     * getting user auth token
     * or null in case if there is no saved token
     */
    fun getSavedTokenUserId(): UserIdAuthTokenLocalStorageDataModel? {
        val sharedPref = activity.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)

        val jsonString = sharedPref.getString(KEY_AUTH_TOKEN_USER_ID, null)
        return if (jsonString == null) {
            null
        } else {
            Gson().fromJson(jsonString, UserIdAuthTokenLocalStorageDataModel::class.java)
        }
    }

}