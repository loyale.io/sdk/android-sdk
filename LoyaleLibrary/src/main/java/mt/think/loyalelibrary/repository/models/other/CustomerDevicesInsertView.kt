package mt.think.loyalelibrary.repository.models.other

data class CustomerDevicesInsertView(
    val deviceName: String?,
    val deviceVersion: String?,
    val osName: String?,
    val osVersion: String?,
    val appName: String?,
    val appVersion: String?,
    val comments: String?
)
