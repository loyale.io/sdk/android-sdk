package mt.think.loyalelibrary.repository.models.other

data class CategoryResultView (
    val id: String,
    val name: String?,
    val imageUrl: String?
        )