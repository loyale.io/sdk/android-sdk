package mt.think.loyalelibrary.repository.models.api_body

data class ApiChangePasswordDataClass(
    val id: String,
    val currentPassword: String,
    val newPassword: String
)
