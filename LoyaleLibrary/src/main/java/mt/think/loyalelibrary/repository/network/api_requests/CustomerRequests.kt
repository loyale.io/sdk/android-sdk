package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_body.ApiCustomerInsertView
import mt.think.loyalelibrary.repository.models.api_body.ApiRequestRegisterPushToken
import mt.think.loyalelibrary.repository.models.api_body.ApiCustomerPutView
import mt.think.loyalelibrary.repository.models.api_response.ApiCustomerResultView
import mt.think.loyalelibrary.repository.models.api_response.ApiCustomerWithAdditionalFieldsResultView
import mt.think.loyalelibrary.repository.models.api_response.ApiPushTokenIdResultView
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling
import okhttp3.MultipartBody
import okhttp3.RequestBody

open class CustomerRequests(
    private val baseUrl: String,
    private val applicationContext: Context
) {

    suspend fun postFirebaseToken(
        schemeId: String,
        customerData: ApiRequestRegisterPushToken
    ): ResponseHolder<ApiPushTokenIdResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .postFirebaseToken(schemeId, customerData)
        }
    }

    suspend fun deleteFirebaseToken(
        authToken: String,
        schemeId: String,
        notificationToken: String
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .deleteFirebaseToken(BEARER + authToken, schemeId, notificationToken)
        }
    }

    suspend fun createCustomer(
        token: String,
        schemeId: String,
        customerData: ApiCustomerInsertView
    ): ResponseHolder<ApiCustomerResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .createCustomer(BEARER + token, schemeId, customerData)
        }
    }

    suspend fun getCustomer(
        token: String,
        schemeId: String,
        customerId: String
    ): ResponseHolder<ApiCustomerResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getCustomer(BEARER + token, schemeId, customerId)
        }
    }

    suspend fun updateCustomer(
        token: String,
        schemeId: String,
        customerData: ApiCustomerPutView
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .updateCustomer(BEARER + token, schemeId, customerData)
        }
    }

    suspend fun getCustomerAdditional(
        token: String,
        schemeId: String,
        customerId: String
    ): ResponseHolder<ApiCustomerWithAdditionalFieldsResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getCustomerAdditional(BEARER + token, schemeId, customerId)
        }
    }

    suspend fun uploadProfileImage(
        token: String,
        schemeId: String,
        customerId: String,
        description: RequestBody,
        file: MultipartBody.Part
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl).uploadProfilePicture(
                BEARER + token,
                schemeId,
                customerId,
                description,
                file
            )
        }
    }

    suspend fun joinScheme(
        token: String,
        schemeId: String,
        customerId: String
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .joinScheme(BEARER + token, schemeId, customerId)
        }
    }

    suspend fun leaveScheme(
        token: String,
        schemeId: String,
        customerId: String
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .leaveScheme(BEARER + token, schemeId, customerId)
        }
    }

    suspend fun suspendUserAccount(
        schemeId: String,
        id: String
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .suspendUserAccount(schemeId, id)
        }
    }

}