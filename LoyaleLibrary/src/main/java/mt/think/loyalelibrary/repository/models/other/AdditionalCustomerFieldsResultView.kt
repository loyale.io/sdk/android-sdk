package mt.think.loyalelibrary.repository.models.other

data class AdditionalCustomerFieldsResultView(
    val customerId: String?,
    val id: String?,
    val schemeId: String?,
    val name: String?,
    val key: String?,
    val value: String?,
    val updatedDate: String,
    val createdDate: String,
    val internal: Boolean
)
