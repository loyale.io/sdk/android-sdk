package mt.think.loyalelibrary.repository.models.api_body

data class ApiRequestRegisterPushToken(
    val customerId: String,
    val token: String
)
