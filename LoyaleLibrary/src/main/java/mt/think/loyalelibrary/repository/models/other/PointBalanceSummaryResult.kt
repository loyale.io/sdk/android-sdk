package mt.think.loyalelibrary.repository.models.other

data class PointBalanceSummaryResult (
    val pointsValue: Double,
    val monetaryValue: Double
        )