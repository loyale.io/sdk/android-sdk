package mt.think.loyalelibrary.repository.models.api_response

data class ApiGetGainRateResultView (
    val gainRate: Double,
    val rounding: Int // enum 0-3
        )