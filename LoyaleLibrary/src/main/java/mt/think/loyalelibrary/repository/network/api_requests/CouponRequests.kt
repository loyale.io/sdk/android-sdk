package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_response.ApiCustomerCouponResultView
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling

open class CouponRequests(
    private val baseUrl: String,
    private val applicationContext: Context
) {

    suspend fun getCouponLinked(
        id: String,
        schemeId: String,
        token: String
    ): ResponseHolder<ApiCustomerCouponResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getCouponLinked(authHeader = BEARER + token, id = id, schemeId = schemeId)
        }
    }

    suspend fun getCouponsLinkedWithFilter(
        schemeId: String,
        token: String,
        filters: String,
        sorts: String,
        page: Int,
        pageSize: Int
    ): ResponseHolder<ArrayList<ApiCustomerCouponResultView>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getCouponsLinkedWithFilter(
                    authHeader = BEARER + token,
                    schemeId = schemeId,
                    filters = filters,
                    sorts = sorts,
                    page = page,
                    pageSize = pageSize
                )
        }
    }

}