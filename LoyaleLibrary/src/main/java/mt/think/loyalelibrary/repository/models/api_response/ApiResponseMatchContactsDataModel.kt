package mt.think.loyalelibrary.repository.models.api_response

data class ApiResponseMatchContactsDataModel (
    val contactMatches: ArrayList<String>
)