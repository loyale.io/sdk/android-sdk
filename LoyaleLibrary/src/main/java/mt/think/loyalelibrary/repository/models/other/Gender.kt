package mt.think.loyalelibrary.repository.models.other

import java.util.*

enum class Gender(val textValue: String, val indexValue: Int) {
    MALE("Male", 0),
    FEMALE("Female", 1),
    NON_BINARY("Other", 2),
    UNSPECIFIED("Unspecified", 3)
}

fun getGenderByStringName(name: String): Gender {
    return when (name.lowercase(Locale.getDefault())) {
        Gender.FEMALE.textValue.lowercase(Locale.getDefault()) -> Gender.FEMALE
        Gender.MALE.textValue.lowercase(Locale.getDefault()) -> Gender.MALE
        Gender.NON_BINARY.textValue.lowercase(Locale.getDefault()) -> Gender.NON_BINARY
        else -> Gender.UNSPECIFIED
    }
}

fun getGenderNamesArray(): Array<String> {
    return arrayOf(
        Gender.MALE.textValue,
        Gender.FEMALE.textValue,
        Gender.NON_BINARY.textValue
    )
}

fun getGenderByIndex(genderIndex: Int): Gender {
    return when (genderIndex) {
        Gender.FEMALE.indexValue -> {
            Gender.FEMALE
        }
        Gender.MALE.indexValue -> {
            Gender.MALE
        }
        Gender.NON_BINARY.indexValue -> {
            Gender.NON_BINARY
        }
        else -> {
            Gender.UNSPECIFIED
        }
    }
}
