package mt.think.loyalelibrary.repository.models.other

data class ProblemDetails(
    val type: String?,
    val title: String?,
    val status: Int?,
    val detail: String?,
    val instance: String?,
    val extensions: Any?
)
