package mt.think.loyalelibrary.repository.models.api_body

data class ApiRequestMatchContactsDataModel (
    val contacts: ArrayList<String>
    )