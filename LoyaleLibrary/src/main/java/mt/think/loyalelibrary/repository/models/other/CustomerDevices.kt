package mt.think.loyalelibrary.repository.models.other

data class CustomerDevices(
    val id: String,
    val customerId: String?,
    val schemeId: String,
    val deviceName: String?,
    val deviceVersion: String?,
    val osName: String?,
    val osVersion: String?,
    val appName: String?,
    val appVersion: String?,
    val comments: String?,
    val firstDevice: Boolean?,
    val createdBy: String?,
    val updatedBy: String?,
    val createdDate: String,
    val updatedDate: String
)
