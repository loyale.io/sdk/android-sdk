package mt.think.loyalelibrary.repository.models.api_response

data class ApiLevelResultView (
    val id: String,
    val tierId: String,
    val schemeId: String,
    val name: String?,
    val pointAllocationPerCurrency: Double,
    val threshold: Int,
    val updatedDate: String,
    val createdDate: String,
    val joinByQRCode: Boolean,
    val percentageDiscount: Double,
    val externalRef: String?
        )