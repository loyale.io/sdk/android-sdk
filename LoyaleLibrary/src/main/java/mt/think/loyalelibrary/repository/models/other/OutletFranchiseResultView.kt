package mt.think.loyalelibrary.repository.models.other

data class OutletFranchiseResultView (
    val id: String,
    val name: String?,
    val description: String?,
    val schemeId: String,
    val imageUrl: String?,
    val updatedDate: String,
    val createdDate: String,
    val hidden: Boolean
        )