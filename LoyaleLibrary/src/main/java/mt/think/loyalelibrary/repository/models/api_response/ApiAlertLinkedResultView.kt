package mt.think.loyalelibrary.repository.models.api_response

import mt.think.loyalelibrary.repository.models.other.AlertResultView

data class ApiAlertLinkedResultView(
    val id: String,
    val alertId: String,
    val alert: AlertResultView,
    val customerId: String?,
    val status: Int, // enum 0-2
    val isDeleted: Boolean,
    val createdDate: String
    )
