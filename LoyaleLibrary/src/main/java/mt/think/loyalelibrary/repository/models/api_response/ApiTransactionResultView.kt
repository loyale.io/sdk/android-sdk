package mt.think.loyalelibrary.repository.models.api_response

import mt.think.loyalelibrary.repository.models.other.CouponResultView
import mt.think.loyalelibrary.repository.models.other.CustomerWithTransactionDetailsResultView
import mt.think.loyalelibrary.repository.models.other.LineItemInsertView

data class ApiTransactionResultView(
    val id: String,
    val value: String?,
    val monetaryValue: Double,
    val saleCurrency: String?,
    val lineItems: ArrayList<LineItemInsertView?>,
    val couponUsed: ArrayList<CouponResultView?>,
    val customerId: String?,
    val customer: ApiCustomerResultView,
    val transactionType: Int, // enum 0-6
    val posId: String?,
    val posType: String?,
    val outletId: String?,
    val outlet: ApiOutletResultView,
    val pointValue: Double,
    val allocatedPointValue: Double,
    val cachedResultantPointValue: Double,
    val deleted: Boolean,
    val externalRefId: String?,
    val description: String?,
    val transactionDate: String,
    val createdBy: String?,
    val createdDate: String,
    val updatedBy: String?,
    val updatedDate: String,
    val schemeId: String,
    val scheme: ApiSchemeResultView,
    val isSplitted: Boolean?,
    val splittedBetween: ArrayList<CustomerWithTransactionDetailsResultView>,
    val isSendPointsAction: Boolean?,
    val targetTransactionId: String?,
    val sourceTransactionId: String?,
    val sourceCustomer: ApiCustomerResultView,
    val hidden: Boolean?,
    val targetCustomer: ApiCustomerResultView,
    val groupId: String?
    )
