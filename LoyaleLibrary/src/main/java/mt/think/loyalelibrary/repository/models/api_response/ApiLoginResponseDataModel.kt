package mt.think.loyalelibrary.repository.models.api_response

data class ApiLoginResponseDataModel(
    val token: String,
    val userId: String,
    val userName: String
)
