package mt.think.loyalelibrary.repository.models.api_body

import mt.think.loyalelibrary.repository.models.other.*

data class ApiCustomerPutView(
    val id: String,
    val firstName: String?,
    val lastName: String?,
    val dob: String?,
    val email: String?,
    val password: String?,
    val areaCode: String?,
    val mobileNumber: String?,
    val addressLine1: String?,
    val addressLine2: String?,
    val town: String?,
    val state: String?,
    val postCode: String?,
    val country: String?,
    val gender: Int, // enum 0-3
    val barCode: String?,
    val marketingSub: Boolean?,
    val detailedLevels: ArrayList<LegacyCustomerLevelView?>,
    val alerts: ArrayList<String?>,
    val profileImageUrl: String?,
    val externalRefId: String?,
    val parentId: String?
)
