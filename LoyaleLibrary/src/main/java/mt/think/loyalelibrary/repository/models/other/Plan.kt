package mt.think.loyalelibrary.repository.models.other

data class Plan(
    val id: String,
    val name: String?,
    val rank: Int,
    val monthlyBasePrice: Double,
    val yearlyBasePrice: Double,
    val agents: Int,
    val customers: Int,
    val pushNotifications: Int,
    val emails: Int,
    val sms: Int,
    val points: Boolean,
    val referrals: Boolean,
    val coupons: Boolean,
    val pointsExpiry: Boolean,
    val hidden: Boolean,
    val multipleGroups: Boolean,
    val multipleBrands: Boolean,
    val api: Boolean,
    val walletApp: Boolean,
    val posApp: Boolean,
    val integrations: Int,
    val sendPoints: Boolean,
    val splitTransactions: Boolean,
    val updatedDate: String,
    val createdDate: String
)