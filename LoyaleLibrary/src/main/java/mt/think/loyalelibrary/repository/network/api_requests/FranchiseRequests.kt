package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_response.ApiFranchiseResultView
import mt.think.loyalelibrary.repository.models.api_response.ApiOutletResultView
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling

open class FranchiseRequests(private val baseUrl: String, private val applicationContext: Context) {
    suspend fun getFranchise(
        id: String,
        schemeId: String,
        token: String
    ): ResponseHolder<ApiFranchiseResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getFranchise(id = id, schemeId = schemeId, authHeader = BEARER + token)
        }
    }

    suspend fun getFranchiseWithFilter(
        token: String,
        schemeId: String,
        filters: String,
        sorts: String,
        page: Int,
        pageSize: Int
    ): ResponseHolder<ArrayList<ApiFranchiseResultView>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getFranchiseWithFilter(
                    authHeader = BEARER + token,
                    schemeId = schemeId,
                    filters = filters,
                    sorts = sorts,
                    page = page,
                    pageSize = pageSize
                )
        }
    }
}