package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_body.ApiRequestMatchContactsDataModel
import mt.think.loyalelibrary.repository.models.api_body.ApiRequestMessage
import mt.think.loyalelibrary.repository.models.api_response.ApiResponseMatchContactsDataModel
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling

open class OtherRequests(private val baseUrl: String, private val applicationContext: Context) {

    suspend fun sendMessage(
        token: String,
        schemeId: String,
        message: ApiRequestMessage
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .sendMessage(schemeId = schemeId, authHeader = BEARER + token, message = message)
        }
    }

    suspend fun matchContactsWithSchemeMembers(
        schemeId: String,
        token: String,
        phoneNumbers: ArrayList<String>
    ): ResponseHolder<ApiResponseMatchContactsDataModel?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .matchContactsWithSchemeMembers(
                    schemeId, BEARER + token, ApiRequestMatchContactsDataModel(phoneNumbers)
                )
        }
    }
}